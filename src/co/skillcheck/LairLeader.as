import co.skillcheck.cooldowntracker.CooldownTracker;
import co.skillcheck.lairshare.LairShare;
import co.skillcheck.autoaccept.AutoAccept;

import com.GameInterface.Game.Character

class co.skillcheck.LairLeader
{
	private static var LAIR_LEADER_TITLE = "Lair Leader: ";

	public static function main(swfRoot:MovieClip):Void
	{
		var lairLeader:LairLeader = new LairLeader(swfRoot);
		var cooldownTracker:CooldownTracker = new CooldownTracker();
		var lairShare:LairShare = new LairShare();
		var autoAccept:AutoAccept = new AutoAccept();

		swfRoot.onLoad = function()
		{
			lairLeader.onLoad();
			cooldownTracker.onLoad();
			lairShare.onLoad();
			autoAccept.onLoad();
		};
		swfRoot.onUnload = function()
		{
			lairLeader.onUnload();
			cooldownTracker.onUnload();
			lairShare.onUnload();
			autoAccept.onUnload();
		};
	}

	public function LairLeader()
	{

	}

	public function onLoad():Void
	{
		
	}

	public function onUnload():Void
	{

	}
}