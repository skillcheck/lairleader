import co.skillcheck.data.LairDataSource;
import co.skillcheck.cooldowntracker.objects.Lair;
import co.skillcheck.cooldowntracker.objects.LairRowEntry;
import com.GameInterface.Quest;
import com.GameInterface.QuestsBase;
import com.GameInterface.Utils;
import mx.utils.Delegate;
import co.skillcheck.objects.LairLeaderSignals;

class co.skillcheck.data.LairRepository
{
	private static var LAIR_REPO_TITLE = "Lair Repo: ";

	var lairDataSource:LairDataSource;

	public function LairRepository()
	{
		lairDataSource = new LairDataSource();
	}

	public function getLairRowData(idx):LairRowEntry
	{
		var lair:Lair = lairDataSource.getLairByIndex(idx);
		return LairRowEntry.create(lair.getLairId(), lair.getLairName(), lair.isLairOnCooldown(), lair.getExpirationTime());
	}

	public function getLairCount():Number
	{
		return lairDataSource.getLairCount();
	}

	public function updateLairData():Void
	{
		var questsOnCooldown = QuestsBase.GetAllQuestsOnCooldown();

		for (var i in questsOnCooldown)
		{
			var cdQuest:Quest = questsOnCooldown[i];

			var lair:Lair = lairDataSource.getLairByQuestId(cdQuest.m_ID);

			if (lair != null)
			{
				lair.setLairOnCooldown(true);
				lair.addQuestCompleteToLair();
				updateLairToOnCooldown(lair, cdQuest);
			}
		}
	}
	
	public function isQuestIdLairQuest(questId:Number):Boolean
	{
		return lairDataSource.isQuestIdLairQuest(questId);
	}

	private function updateLairToOnCooldown(lair:Lair, cdQuest:Quest):Void
	{
		if (cdQuest.m_CooldownExpireTime > lair.getExpirationTime())
		{
			lair.updateExpirationTime(cdQuest.m_CooldownExpireTime);
		}
	}

	/*public function updateLairDataTest():Void
	{
		var testCDQuest = new Array();
		var testQuest:Quest = new Quest();
		testQuest.m_ID = 4056;
		testQuest.m_CooldownExpireTime = 1605790359;
		testCDQuest[0] = testQuest;

		for (var i in testCDQuest)
		{
			var cdQuest:Quest = testCDQuest[i];

			var lair:Lair = lairDataSource.getLairByQuestId(cdQuest.m_ID);

			if (lair != null)
			{
				lair.setLairOnCooldown(true);
				updateLairToOnCooldown(lair, cdQuest);
			}
		}
	}*/
}