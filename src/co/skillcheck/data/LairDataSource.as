import co.skillcheck.cooldowntracker.objects.Lair;
import com.GameInterface.Utils;

class co.skillcheck.data.LairDataSource
{
	private static var LAIR_DATASOURCE_TITLE = "Lair DataSource: ";

	private var _lairData:Array;

	public function LairDataSource()
	{
		_lairData = new Array();
		//Kingsmouth
		_lairData[0] = Lair.create("KM", "Kingsmouth", [3434, 3445, 3422]);

		//Blue Mountain
		_lairData[1] = Lair.create("BM", "Blue Mountain", [3447, 3424, 3439]);

		//Savage Coast
		_lairData[2] = Lair.create("SC", "Savage Coast", [3446, 3436, 3423]);

		//Scored Desert
		_lairData[3] = Lair.create("SD", "Scored Desert", [3448, 3426, 3428]);

		//City of the Sun God
		_lairData[4] = Lair.create("CotSG", "City of the Sun God", [3449, 3425, 3429]);

		//Beseiged Farmlands
		_lairData[5] = Lair.create("BF", "Beseiged Farmlands", [3413, 3412, 3411]);

		//Shadowy Forest
		_lairData[6] = Lair.create("SF", "Shadowy Forest", [3415, 3416, 3421]);

		//Carpathian Fangs
		_lairData[7] = Lair.create("CF", "Carpathian Fangs", [3418, 3419, 3414]);
		
		//Kaidan
		_lairData[8] = Lair.create("KD", "Kaidan", [4056, 4054, 4064]);
	}

	public function getLairCount():Number
	{
		return _lairData.length;
	}
	
	public function getLairByIndex(idx:Number):Lair
	{
		return _lairData[idx];
	}

	public function getLairByQuestId(cdQuestId:Number):Lair
	{
		for (var i in _lairData)
		{
			var lair:Lair = _lairData[i];
			if (lair.isQuestInLair(cdQuestId)) return lair;
		}
	}
	
	public function isQuestIdLairQuest(questId:Number):Boolean 
	{
		for (var i in _lairData) {
			var lair:Lair = _lairData[i];
			if (lair.isQuestInLair(questId))
			{
				return true;
			}
		}
		return false;
	}
}