import co.skillcheck.objects.LairRowEntry;
import com.Utils.LDBFormat;
import com.GameInterface.Utils;

class co.skillcheck.cooldowntracker.objects.LairRowEntry
{
	private static var LAIR_ROW_TITLE = "Lair Row: ";

	var _id:Number;
	var _name:String;
	var _onCooldown:Boolean;
	private var _expiration:Number;

	public function LairRowEntry()
	{
		_id = 0;
		_name = null;
		_onCooldown = false;
		_expiration = 0;
	}

	public static function create(id, name, onCooldown, expiration):LairRowEntry
	{
		var row:LairRowEntry = new LairRowEntry();
		row._id = id;
		row._name = name;
		row._onCooldown = onCooldown;
		row._expiration = expiration;
		return row;
	}

	public function getExpirationString():String
	{
		if (!_onCooldown)
		{
			return LDBFormat.LDBGetText("MiscGUI", "LockoutTimers_Available");
		}

		var currentTime = new Date();
		currentTime = currentTime.getTime();
		var timeLeft = (_expiration*1000 - currentTime)/1000;

		var totalMinutes = timeLeft / 60;
		var totalHours = totalMinutes / 60;
		var totalDays = totalHours / 24;
		
		var daysString = String(Math.floor(totalDays));
		
		var hours = totalHours % 24
		var hoursString = String(Math.floor(hours));
		
		var minutes = totalMinutes % 60;
		var minutesString = String(Math.floor(minutes));
		
		var timeString = "";
		if (daysString > 0) {
			timeString = LDBFormat.Printf(LDBFormat.LDBGetText("Gamecode", "TimeFormatDaysShort"), daysString) + " ";
		}
		if (hoursString > 0)
		{
			timeString += LDBFormat.Printf(LDBFormat.LDBGetText("Gamecode", "TimeFormatHoursShort"), hoursString) + " ";
		}
		if (minutesString > 0)
		{
			timeString += LDBFormat.Printf(LDBFormat.LDBGetText("Gamecode", "TimeFormatMinutesShort"), minutesString);
		}
		return timeString;
	}

}