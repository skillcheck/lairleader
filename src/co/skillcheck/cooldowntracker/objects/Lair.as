import GUI.SkillHiveSimple.BoostClip;
import co.skillcheck.objects.Lair;
import com.GameInterface.Quest;

class co.skillcheck.cooldowntracker.objects.Lair
{

	private var _id:String;
	private var _name:String;
	private var _questIds:Array;
	private var _isOnCooldown:Boolean;
	private var _expiration:Number;
	private var _isLairComplete:Boolean;
	private var _lairQuestsCompletedCount:Number;

	public function Lair()
	{
		_id = null;
		_name = null;
		_questIds = new Array();
		_isOnCooldown = false;
		_expiration = 0;
		_isLairComplete = false;
		_lairQuestsCompletedCount = 0;
	}

	public static function create(id:String, name:String, questIds:Array):Lair
	{
		var lair:Lair = new Lair();
		lair._id = id;
		lair._name = name;
		lair._questIds = questIds;
		return lair;
	}

	public function getLairId():String
	{
		return _id;
	}

	public function getLairName():String
	{
		return _name;
	}

	public function getLairQuestIds():Array
	{
		return _questIds;
	}

	public function isLairOnCooldown():Boolean
	{
		return _isOnCooldown;
	}

	public function setLairOnCooldown(isOnCD:Boolean):Void
	{
		_isOnCooldown = isOnCD;
	}

	public function getExpirationTime():Number
	{
		return _expiration;
	}

	public function updateExpirationTime(time:Number):Void
	{
		_expiration = time;
	}

	public function isQuestInLair(cdQuestId:Number):Boolean 
	{
		for (var i in _questIds) {
			if (_questIds[i] == cdQuestId) {
				return true;
			}
		}
		return false;
	}

	public function addQuestCompleteToLair():Void 
	{
		_lairQuestsCompletedCount++;
	}
	
	public function getLairQuestsCompleteCount():Number
	{
		return _lairQuestsCompletedCount;
	}
}