import co.skillcheck.data.LairRepository;
import co.skillcheck.cooldowntracker.objects.LairRowEntry;
import com.Utils.Colors;

class co.skillcheck.cooldowntracker.views.LairRow extends MovieClip
{

	public function LairRow()
	{

	}

	public static function create(lair:LairRowEntry, depth):MovieClip
	{
		var daClip:MovieClip = _root.lockouttimers.m_Window.m_Content.m_DarkAgartha;
		
		var rowClip:MovieClip = _root.lockouttimers.m_Window.m_Content.m_LairRows["m_Row" + lair._id];
		if (rowClip == undefined)
		{
			rowClip = daClip.duplicateMovieClip("m_Row" + lair._id, depth++);
			rowClip.m_Character.deleteMovieClip();
			rowClip.m_Name.text = lair._name;
			rowClip.m_Lockout.text = lair.getExpirationString();

			if (!lair._onCooldown)
			{
				rowClip.m_Lockout.textColor = Colors.e_ColorGreen;
			}
			else
			{
				rowClip.m_Lockout.textColor = Colors.e_ColorRed;
			}
		}
		return rowClip;
	}
}