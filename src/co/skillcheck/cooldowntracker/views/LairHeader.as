
class co.skillcheck.cooldowntracker.views.LairHeader extends MovieClip
{

	public function LairHeader()
	{
		
	}

	public static function create():MovieClip
	{
		var contentClip:MovieClip = _root.lockouttimers.m_Window.m_Content;
		var raidClip:TextField = _root.lockouttimers.m_Window.m_Content.m_RaidsHeader;
		var depth = _root.lockouttimers.m_Window.m_Content.getNextHighestDepth();

		var m_LairsHeader:MovieClip = contentClip.createEmptyMovieClip("m_LairsHeader", depth);

		var path = "LairLeader\\assets\\img\\HeaderBG.png";
		var m_HeaderLoader:MovieClipLoader = new MovieClipLoader();
		m_HeaderLoader.loadClip(path, m_LairsHeader);
		
		m_LairsHeader._y = 310;
		m_LairsHeader._x = 10;

		var m_HeaderText:TextField = m_LairsHeader.createTextField("m_HeaderText", depth++, 0, 0, 100, 25);
		m_HeaderText.background = false;
		m_HeaderText.text = "Lairs";

		m_HeaderText.setTextFormat(raidClip.getTextFormat());
		return m_LairsHeader;
	}
}