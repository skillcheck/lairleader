﻿import co.skillcheck.cooldowntracker.CooldownTracker;
import co.skillcheck.cooldowntracker.objects.Lair;
import co.skillcheck.data.LairRepository;
import co.skillcheck.cooldowntracker.objects.LairRowEntry;
import com.GameInterface.DistributedValue;
import mx.utils.Delegate;
import com.Utils.LDBFormat;
import com.Utils.Colors;
import com.GameInterface.Quest;
import GUI.Mission.MissionSignals;
import com.GameInterface.Utils;
import co.skillcheck.cooldowntracker.views.LairHeader;
import co.skillcheck.cooldowntracker.views.LairRow;

class co.skillcheck.cooldowntracker.CooldownTracker
{
	private static var CD_TRACKER_TITLE = "CooldownTracker: ";

	private var lockoutsOpened:DistributedValue;
	private var lairRepository:LairRepository;
	private var firstTimeRun:Boolean;

	public function CooldownTracker()
	{
		lockoutsOpened = DistributedValue.Create("lockoutTimers_window");
		lairRepository = new LairRepository();
		firstTimeRun = true;
	}

	public function onLoad():Void
	{
		lockoutsOpened.SignalChanged.Connect(updateLairData, this);
		MissionSignals.SignalMissionReportSent.Connect(updateLairData, this);
		if (firstTimeRun)
		{
			addLairHeader();
			updateLairData();
			firstTimeRun = false;
		}
	}

	public function onUnload():Void
	{
		lockoutsOpened.SignalChanged.Disconnect(updateLairData, this);
		MissionSignals.SignalMissionReportSent.Disconnect(updateLairData, this);
	}

	private function addLairHeader()
	{
		var contentClip:MovieClip = _root.lockouttimers.m_Window.m_Content;
		
		if (lockoutsOpened.GetValue())
		{
			if (_root.lockouttimers.m_Window.m_Content.m_RaidsHeader)
			{
				var m_LairsHeader:MovieClip = _root.lockouttimers.m_Window.m_Content.m_LairsHeader;
				if (m_LairsHeader == undefined)
				{
					m_LairsHeader = LairHeader.create();
					contentClip.attachMovie(m_LairsHeader._name, "m_LairsHeader", contentClip.getNextHighestDepth());
				}
				setTimeout(Delegate.create(this, addLairLockouts), 50);
			}
			else
			{
				setTimeout(Delegate.create(this, addLairHeader), 50);
			}
		}
	}

	private function addLairLockouts()
	{
		var contentClip:MovieClip = _root.lockouttimers.m_Window.m_Content;
		var rowY = contentClip._height + 5;

		for (var i = 0; i < lairRepository.getLairCount(); i++;)
		{
			var lair:LairRowEntry = lairRepository.getLairRowData(i);
			var rowClip:MovieClip = _root.lockouttimers.m_Window.m_Content["m_Row" + lair._id];

			if (rowClip == undefined)
			{
				rowClip = LairRow.create(lair, contentClip.getNextHighestDepth());
				rowClip._y = rowY;
				rowY += 20;
				contentClip.attachMovie(rowClip._name, "m_Row" + lair._id, contentClip.getNextHighestDepth());
			}
		}
		
		_root.lockouttimers.m_Window.m_Background._height = 580;
	}

	private function updateLairData():Void
	{
		lairRepository.updateLairData();
		addLairHeader();
	}

	/*private function updateLairDataTest():Void
	{
		Utils.PrintChatText(LAIR_LEADER_TITLE + "Lair Data Test");
		lairRepository.updateLairDataTest();
		addLairHeader();
	}*/
}