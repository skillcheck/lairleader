import co.skillcheck.data.LairRepository;
import com.GameInterface.Quest;
import com.GameInterface.VicinitySystem;
import com.GameInterface.Game.Dynel;
import com.Utils.ID32;
import com.GameInterface.QuestGiver;
import com.GameInterface.QuestsBase;
import com.GameInterface.Quests;
import com.GameInterface.Utils;

class co.skillcheck.autoaccept.AutoAccept
{
	private static var AUTO_ACCEPT_TITLE = "Auto Accept: ";

	var lairRepository:LairRepository;

	public function AutoAccept()
	{
		lairRepository = new LairRepository();
	}

	public function onLoad():Void
	{
		VicinitySystem.SignalDynelEnterVicinity.Connect(checkQuestGiver, this)
	}

	public function onUnload():Void
	{
		VicinitySystem.SignalDynelEnterVicinity.Disconnect(checkQuestGiver, this)
	}

	private function checkQuestGiver(dynelId:ID32):Void
	{
		var dynel:Dynel = Dynel.GetDynel(dynelId);
		if (dynel.IsMissionGiver())
		{
			var questGiver:QuestGiver = QuestsBase.GetQuestGiver(dynelId, true);
			for (var i in questGiver.m_AvailableQuests)
			{
				var quest:Quest = questGiver.m_AvailableQuests[i];
				if (lairRepository.isQuestIdLairQuest(quest.m_ID))
				{
					if (!doesCharacterAlreadyHaveQuest(quest.m_ID))
					{
						acceptLairQuest(quest.m_ID, dynelId);
					}
				}
			}
		}
	}

	private function doesCharacterAlreadyHaveQuest(questId:Number):Boolean
	{
		var activeQuests = QuestsBase.GetAllActiveQuests();
		for (var i in activeQuests)
		{
			var quest:Quest = activeQuests[i];
			if (quest.m_ID == questId)
			{
				return true
			}
		}
		return false;
	}

	private function acceptLairQuest(questId:Number, dynelId):Void
	{
		QuestsBase.AcceptQuestFromQuestgiver(questId, dynelId)
	}
}