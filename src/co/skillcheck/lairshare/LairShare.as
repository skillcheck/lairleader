import co.skillcheck.data.LairRepository;
import com.GameInterface.DistributedValue;
import co.skillcheck.lairshare.views.LairShareButton;
import com.GameInterface.Utils;
import mx.utils.Delegate;
import com.GameInterface.Quest;
import com.GameInterface.QuestsBase;
import Chat

class co.skillcheck.lairshare.LairShare
{

	private static var LAIR_SHARE_TITLE = "LairShare: ";

	private var movieClipLoader:MovieClipLoader;

	private var journalOpen:DistributedValue;
	private var firstTimeRun:Boolean;

	private var m_LSButton:MovieClip;
	
	private var lairRepository:LairRepository;

	public function LairShare()
	{
		journalOpen = DistributedValue.Create("mission_journal_window");
		firstTimeRun = true;
		lairRepository = new LairRepository();
	}

	public function onLoad():Void
	{
		movieClipLoader = new MovieClipLoader();
		movieClipLoader.addListener(this);

		journalOpen.SignalChanged.Connect(addLairShareButton, this);

		if (firstTimeRun)
		{
			addLairShareButton();
			firstTimeRun = false;
		}
	}

	public function onUnload():Void
	{
		movieClipLoader.removeListener(this);
		movieClipLoader.unloadClip(m_LSButton);

		journalOpen.SignalChanged.Disconnect(addLairShareButton, this);

		m_LSButton.onPress = undefined;
	}

	private function addLairShareButton():Void
	{
		if (journalOpen.GetValue())
		{
			var contentClip:MovieClip = _root.missionjournalwindow.m_Window.m_Content;
			if (contentClip != undefined)
			{
				var path = "LairLeader\\assets\\img\\LairShareButton.png";
				m_LSButton = contentClip.createEmptyMovieClip("u_m_LSButton", contentClip.getNextHighestDepth());
				movieClipLoader.loadClip(path, m_LSButton);
			}
			else
			{
				setTimeout(Delegate.create(this, addLairShareButton), 50);
			}
		}
	}

	private function onLoadComplete(target:MovieClip):Void
	{
		if (target == m_LSButton)
		{
			m_LSButton._x = 280;
			m_LSButton._y = 0;
			m_LSButton._xscale = 50;
			m_LSButton._yscale = 38;

			m_LSButton.onPress = Delegate.create(this, shareLairQuests);
		}
	}

	private function shareLairQuests():Void
	{
		var activeQuests:Array = QuestsBase.GetAllActiveQuests();
		if (activeQuests != null || activeQuests.length != 0)
		{
			var lairQuestCount = 0;
			for (var i in activeQuests)
			{
				var quest:Quest = activeQuests[i];
				if (lairRepository.isQuestIdLairQuest(quest.m_ID))
				{
					setTimeout(function() { QuestsBase.ShareQuest(quest.m_ID) }, lairQuestCount * 5000);
					lairQuestCount++;
				}
			}
		}
	}
}